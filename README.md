# Recruitment challenge

For recruitment purposes.

## Installation

Clone repository into your local machine, then install dependencies:

```bash
cd recruitment-challenge
yarn install
```

## Usage

```bash
yarn start
```
