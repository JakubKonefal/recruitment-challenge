import Contacts from './Contacts/Contacts';
import classes from './App.module.css';

const App = () => (
  <main className={classes.App}>
    <Contacts />
  </main>
);

export default App;
