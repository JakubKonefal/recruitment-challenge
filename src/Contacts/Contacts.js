import React, { useState, useEffect } from 'react';
import Contact from './Contact';
import SearchBar from './SearchBar';
import Spinner from '../Spinner/Spinner';
import classes from './Contacts.module.css';
import axios from 'axios';

const Contacts = () => {
  const [allUsersList, setAllUsersList] = useState([]);
  const [filteredUsersList, setFilteredUsersList] = useState([]);
  const [loading, setLoading] = useState(true);
  const [selectedUsers, setSelectedUsers] = useState([]);
  const [searchedUser, setSearchedUser] = useState('');

  const getAllUsersList = () => {
    axios
      .get(
        'https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com/users.json'
      )
      .then(({ data }) => {
        const contactsByLastName = data.sort((a, b) =>
          a.last_name.localeCompare(b.last_name)
        );
        setAllUsersList(contactsByLastName);
        setLoading(false);
      });
  };

  const handleUserSelect = (id) => {
    const newSelectedUsers = selectedUsers.includes(id)
      ? selectedUsers.filter((item) => item !== id)
      : [...selectedUsers, id];

    setSelectedUsers(newSelectedUsers);
    console.log(newSelectedUsers);
  };

  const handleUsersFilter = ({ target: { value } }) => {
    setSearchedUser(value);
    const filteredUsers = allUsersList.filter((user) =>
      user.first_name.toLowerCase().startsWith(value.toLowerCase()) ||
      user.last_name.toLowerCase().startsWith(value.toLowerCase())
        ? user
        : null
    );
    setFilteredUsersList(filteredUsers);
  };

  useEffect(getAllUsersList, []);

  if (loading) {
    return <Spinner />;
  }

  return (
    <>
      <header className={classes.Header}>
        <h1 className={classes.Header__Title}> Contacts </h1>
      </header>
      <SearchBar
        handleUsersFilter={handleUsersFilter}
        inputValue={searchedUser}
      />
      <ul className={classes.List}>
        {filteredUsersList.length > 0 || searchedUser.length > 0
          ? filteredUsersList.map((user) => (
              <Contact
                key={user.id}
                {...user}
                handleUserSelect={handleUserSelect}
                checked={selectedUsers.includes(user.id)}
              />
            ))
          : allUsersList.map((user) => (
              <Contact
                key={user.id}
                {...user}
                handleUserSelect={handleUserSelect}
                checked={selectedUsers.includes(user.id)}
              />
            ))}
      </ul>
    </>
  );
};

export default Contacts;
