import React from 'react';
import Checkbox from '@material-ui/core/Checkbox';
import Avatar from '@material-ui/core/Avatar';
import classes from './Contact.module.css';
import defaultAvatar from '../assets/default-avatar.png';

const Contact = ({
  id,
  first_name,
  last_name,
  email,
  avatar,
  checked,
  handleUserSelect,
}) => (
  <li className={classes.Contact} onClick={() => handleUserSelect(id)}>
    <div className={classes.Contact__Avatar}>
      <Avatar
        className={classes.Contact__Avatar_Img}
        src={avatar || defaultAvatar}
        alt={`${first_name} ${last_name}`}
      />
    </div>
    <div className={classes.Contact__Info}>
      <span className={classes.Contact__Fullname}>
        {`${first_name} ${last_name}`}
      </span>
      <span className={classes.Contact__Email}> {email} </span>
    </div>

    <div className={classes.Contact__CheckboxWraper}>
      <Checkbox checked={checked} />
    </div>
  </li>
);

export default Contact;
