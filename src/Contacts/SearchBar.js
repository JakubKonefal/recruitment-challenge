import React from 'react';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
import Search from '@material-ui/icons/Search';
import classes from './SearchBar.module.css';

const SearchBar = ({ handleUsersFilter, inputValue }) => (
  <div className={classes.SearchBar}>
    <TextField
      className={classes.SearchBar__Input}
      value={inputValue}
      variant="outlined"
      InputProps={{
        startAdornment: (
          <InputAdornment position="start">
            <Search />
          </InputAdornment>
        ),
      }}
      onChange={handleUsersFilter}
    />
  </div>
);

export default SearchBar;
